## Parts List

Onderdelen in overzichtlijst, met * voor lesmateriaal:

| **Part Name** | **Interfaces** | **Description** | **I2C Address** |
| ----------- | ----------- | ----------- | ----------- |
| **Barometric** | **and** | **Temperature** | **Sensors** |
| DPS310* | I2C, STEMMA QT, Analog | Temp. and Barometric Pressure Sensor | 0x77 |
| PCT2075* | I2C, STEMMA QT, Analog | Temperature Sensor | 0x37 |
|   |   |   |   |
| **Motion** | **and** | **Acceleration** | **Sensors** |
| LSM6D33* | I2C, STEMMA QT, Analog | 6 DoF Accel/Gyro | 0x6A |
|   |   |   |   |
| **OLED** |   |   |   |
| SSD1306* | I2C | OLED 128 x 64 | 0x78 |
|   |   |   |   |
| **Audio** |   |   |   |
| Amp Speaker* | STEMMA 3 Wire, Alligator | Mono Class D Audio Amp 8Ohm 1W Speaker | - |
|   |   |   |   |
| **Helper** |   |   |   |
| TCA9548A | I2C | I2C Address Multiplexer | - |
|   |   |   |   |
| **Proximity** | **and** | **Lux** | **Sensor** |
| VEML7700 | I2C | Lux Sensor | Light Sensor | - |
| VCNL4040* | I2C, STEMMA QT | Proximity and Lux Sensor | 0x60 | 
| VL6180X | I2C, STEMMA QT | Proximity Sensor | - |
|   |   |   |   |
| **GPS** |   |   |   |
| MiniGPS | I2C, STEMMA QT, UART | STEMMA MiniGPS | 0x10 |
|   |   |   |   |
| **Boards** |   |   |   |
| MCP2221* | I2C, STEMMA QT, ADC/DAC, UART, GPIO, USB-C | Computer to Component Connector | - |
|   |   |   |   |
| **Toebehoor** | **Kabels, weerstanden**  | **knoppen, LEDs en**  | **Breadboard**  |
| STEMMA QT Kabel* | I2C, STEMMA QT | 4x kabel, vier-aderig met STEMMA QT stekker  | - |
| Potimeter* | Analoog | Draaiknop | - |
| Drukknoppen* | Digitaal | Verschillende drukknoppen, groot klein | - |
| LED lampjes* | Analoog | Lampjes, verschillende maten | - |
| Lux sensor* | Analoog | Licht sensor | - |
| Weerstanden* | - | Weerstanden om stroom doorvoer te beperken | - |
| Solderless Breadboard* | - | Bord om snel zonder solderen tijdelijk circuit te bouwen | - |
| Breadboard kabels* | - | Kabeltjes (solide kern) om onderdelen op bread met elkaar te verbinden | - | 