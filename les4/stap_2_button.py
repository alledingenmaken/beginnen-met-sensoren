import time
import board
import digitalio

button = digitalio.DigitalInOut(board.G1)
button.direction = digitalio.Direction.INPUT
    
while True:
    print(button.value)
    time.sleep(0.5)