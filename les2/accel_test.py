import time
import board
import busio
from adafruit_lsm6ds import LSM6DS33
 
i2c = busio.I2C(board.SCL, board.SDA)
 
sensor = LSM6DS33(i2c)
 
while True:
    print("(%.2f, %.2f, %.2f)" % (sensor.acceleration))
    print("(%.2f, %.2f, %.2f)" % (sensor.gyro))
    time.sleep(0.5)